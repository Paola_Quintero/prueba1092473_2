/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473.pkg2;

/**
 *
 * @author SergioIván
 */
public class Circulo extends FiguraGeometrica{

    private float radio;
    public Circulo(float radio)
    {
        this.radio=radio;
    }
    @Override
    public float calcularArea() {
      
        
        String temp=Double.toString(Math.PI*Math.pow(radio, 2));
        return Float.parseFloat(temp);
    }

    @Override
    public float calcularPerimetro() {
       
        String temp=Double.toString(2.0*Math.PI*radio);
        return Float.parseFloat(temp);
    }
    
}
